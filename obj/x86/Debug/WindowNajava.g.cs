﻿#pragma checksum "..\..\..\WindowNajava.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "D906DB7E8B3F7C3579CA93DC6D9C2D83"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18444
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace InterfejsiProekt {
    
    
    /// <summary>
    /// WindowNajava
    /// </summary>
    public partial class WindowNajava : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 24 "..\..\..\WindowNajava.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock errorLabel;
        
        #line default
        #line hidden
        
        
        #line 25 "..\..\..\WindowNajava.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock errorLabel1;
        
        #line default
        #line hidden
        
        
        #line 30 "..\..\..\WindowNajava.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox korisnicko;
        
        #line default
        #line hidden
        
        
        #line 31 "..\..\..\WindowNajava.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.PasswordBox lozinka;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/InterfejsiProekt;component/windownajava.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\WindowNajava.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            
            #line 21 "..\..\..\WindowNajava.xaml"
            ((System.Windows.Documents.Hyperlink)(target)).Click += new System.Windows.RoutedEventHandler(this.Hyperlink_Click);
            
            #line default
            #line hidden
            return;
            case 2:
            this.errorLabel = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 3:
            this.errorLabel1 = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 4:
            this.korisnicko = ((System.Windows.Controls.TextBox)(target));
            
            #line 30 "..\..\..\WindowNajava.xaml"
            this.korisnicko.MouseEnter += new System.Windows.Input.MouseEventHandler(this.errorLabel_MouseEnter);
            
            #line default
            #line hidden
            return;
            case 5:
            this.lozinka = ((System.Windows.Controls.PasswordBox)(target));
            
            #line 31 "..\..\..\WindowNajava.xaml"
            this.lozinka.MouseEnter += new System.Windows.Input.MouseEventHandler(this.errorLabel_MouseEnter);
            
            #line default
            #line hidden
            return;
            case 6:
            
            #line 32 "..\..\..\WindowNajava.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.Button_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

