﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data.SqlClient;

namespace InterfejsiProekt
{
    /// <summary>
    /// Interaction logic for WindowNajava.xaml
    /// </summary>
    public partial class WindowNajava : Window
    {
        string id_roditel = string.Empty;
        static  WindowDeca deca;
        public WindowNajava()
        {
            InitializeComponent();
        }
        private void Hyperlink_Click(object sender, RoutedEventArgs e)
        {
            WindowRegistracija r = new WindowRegistracija();
            r.ShowDialog();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (exsist())
            {
                najava();
                this.Close();
            }
            else
            {

                errorLabel1.Visibility = System.Windows.Visibility.Visible;
                errorLabel.Visibility = System.Windows.Visibility.Visible;
                errorLabel.Text = "грешнo корисничко имe";
                errorLabel1.Text = "или лозинка";
            }
        }

        void najava()
        {
            SqlConnection konekcija = new SqlConnection();
            konekcija.ConnectionString = @"Data Source=VLADE\SQLEXPRESS;Initial Catalog=Interfejsi;Integrated Security=True";
            String komanda = "select * from Korisnici";
            SqlCommand command = new SqlCommand(komanda, konekcija);

            try
            {
                konekcija.Open();
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    if (reader["username"].Equals(korisnicko.Text) && reader["password"].Equals(lozinka.Password) && reader["tip"].Equals(1))
                    {
                         deca = new WindowDeca();
                        deca.korisnikId.Text = reader["id"].ToString();
                        this.Close();
                        deca.ShowDialog();
                    }
                    else if (reader["username"].Equals(korisnicko.Text) && reader["password"].Equals(lozinka.Password) && reader["tip"].Equals(0))
                    {
                        MainWindow roditeli = new MainWindow();
                        roditeli.korisnickoIme.Text = reader["username"].ToString();
                        roditeli.korisnickoIme1.Text = reader["username"].ToString();
                        roditeli.korisnickoIme2.Text = reader["username"].ToString();
                        roditeli.korisnickoIme3.Text = reader["username"].ToString();
                        roditeli.idRoditel.Text = reader["id"].ToString();
                        this.Close();
                        roditeli.ShowDialog();
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Не може да се конектира со базата");
                MessageBox.Show(e.Message);
            }
            finally
            {
                konekcija.Close();
            }
        }
        bool exsist()
        {
            bool flag = false;
            SqlConnection konekcija = new SqlConnection();
            konekcija.ConnectionString = @"Data Source=VLADE\SQLEXPRESS;Initial Catalog=Interfejsi;Integrated Security=True";
            String komanda = "select * from Korisnici";
            SqlCommand command = new SqlCommand(komanda, konekcija);

            try
            {
                konekcija.Open();
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    if (reader["username"].Equals(korisnicko.Text) && reader["password"].Equals(lozinka.Password))
                    {
                        flag = true;
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Не може да се конектира со базата");
                MessageBox.Show(e.Message);
            }
            finally
            {
                konekcija.Close();
            }
            return flag;
        }
        private void errorLabel_MouseEnter(object sender, MouseEventArgs e)
        {
            errorLabel1.Visibility = System.Windows.Visibility.Hidden;
            errorLabel.Visibility = System.Windows.Visibility.Hidden;
        }
    }
}
