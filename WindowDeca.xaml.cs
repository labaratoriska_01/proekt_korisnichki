﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data.SqlClient;
using System.Data;
namespace InterfejsiProekt
{
    /// <summary>
    /// Interaction logic for WindowDeca.xaml
    /// </summary>
    public partial class WindowDeca : Window
    {
        string dbConnection = @"Data Source=VLADE\SQLEXPRESS;Initial Catalog=Interfejsi;Integrated Security=True";

        public static bool smeniEnable=false;
        Poraka p;
        public  WindowDeca()
        {
            InitializeComponent();
  
        }

        public void Window_Loaded(object sender, RoutedEventArgs e)
        {
           // MessageBox.Show("oh");
            SqlConnection konekcija = new SqlConnection(dbConnection);

            string slika_url = "SELECT slika_url from korisnici where id ='" + korisnikId.Text + "'";
            SqlCommand komandaSlika = new SqlCommand(slika_url, konekcija);

            try
            {
                konekcija.Open();
                slikaNajavenoDete.Source = new BitmapImage(new Uri(komandaSlika.ExecuteScalar().ToString()));
            }
            catch (Exception err)
            {
                MessageBox.Show("Не може да се конектира со базата");
                MessageBox.Show(err.Message);
            }
            finally
            {
                konekcija.Close();
            }
            
        }

        public  void  listaKontakti_Loaded(object sender, RoutedEventArgs e)
        {
            SqlConnection konekcija = new SqlConnection(dbConnection);

            string najdi_kontakti = "select a.ime,a.prezime,a.slika_url from (select * from Kontakti inner join  Korisnici K on id_kontakt = K.id where id_korisnik = '" + korisnikId.Text + "') a";
            SqlCommand komanda_kontakti = new SqlCommand(najdi_kontakti, konekcija);
            DataSet dtSet = new DataSet();

            try
            {
                konekcija.Open();

                SqlDataAdapter adapterKontakti = new SqlDataAdapter();

                adapterKontakti.SelectCommand = komanda_kontakti;
                adapterKontakti.Fill(dtSet, "Korisnici");
                listaKontakti.DataContext = dtSet;

            }
            catch (Exception err)
            {
                MessageBox.Show("Не може да се конектира со базата");
                MessageBox.Show(err.Message);
            }
            finally
            {
                konekcija.Close();
            }
        }

        public  void listaKontakti_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            btnPratiPoraka.IsEnabled = true;
            if (listaPorakiKontakt != null)
            {
                listaPorakiKontakt.Items.Clear();
            }
            popolniListaSoPoraki();
        }

        private void popolniListaSoPoraki()
        {
            List<ImageSource> sliki = new List<ImageSource>();

            SqlConnection konekcija = new SqlConnection(dbConnection);

            DataRowView drView = (DataRowView)listaKontakti.SelectedItem;
            //MessageBox.Show(drView.Row[2].ToString());
            string slika_kontakt = drView.Row[2].ToString();

            string najdi_kontakt = "select id from Korisnici where slika_url = '" + slika_kontakt + "'";
            SqlCommand komanda_kontakt = new SqlCommand(najdi_kontakt, konekcija);


            DataSet dtSet = new DataSet();

            try
            {
                konekcija.Open();

                string najdi_poraki = "select sodrzina_poraka from PrakjaPrima where id_od = '" + komanda_kontakt.ExecuteScalar() + "'and id_do='" + korisnikId.Text + "' or id_do='" + komanda_kontakt.ExecuteScalar() + "' and id_od='" + korisnikId.Text + "'";

                SqlCommand komanda_poraki = new SqlCommand(najdi_poraki, konekcija);

                SqlDataReader dr = komanda_poraki.ExecuteReader();

                DataTable schemaTable = dr.GetSchemaTable();

                DataTable dataTable = new DataTable();

                for (int i = 0; i <= schemaTable.Rows.Count - 1; i++)
                {

                    DataRow dataRow = schemaTable.Rows[i];

                    string columnName = dataRow["ColumnName"].ToString();
                    DataColumn column = new DataColumn(columnName);
                    dataTable.Columns.Add(column);

                }

                dtSet.Tables.Add(dataTable);

                while (dr.Read())
                {
                    Image im = new Image();
                    DataRow dataRow = dataTable.NewRow();
                    // ListItem ii = new ListItem();
                    // listaPorakiKontakt.Items.Add(ii);
                    for (int i = 0; i <= dr.FieldCount - 1; i++)
                    {
                        // <Image Height="70" Width="70" Source="{Binding Path=sodrzina_poraka}"/>
                        String[] imgs = dr.GetValue(i).ToString().Split(',');
                        GridViewColumn kolona = new GridViewColumn();

                        foreach (String s in imgs)
                        {
                            im = new Image();
                            im.Height = Double.Parse("70");
                            im.Width = Double.Parse("70");
                            //  MessageBox.Show(s);
                            im.Source = (ImageSource)new ImageSourceConverter().ConvertFromString(s);
                            if (im.Source != null)
                            {


                                listaPorakiKontakt.Items.Add(im);
                                //   MessageBox.Show("vo !null");
                            }
                            else
                            {
                                // MessageBox.Show("vo null");
                            }
                        }
                        /*
                        foreach (String s in imgs)
                        {
                            im = new Image();
                            MessageBox.Show(s);

                            im.Source = (ImageSource)new ImageSourceConverter().ConvertFromString(s);
                            if (im.Source != null)
                            {
                                //int ind=listaPorakiKontakt.Items.CurrentPosition;
                                ListItem curr = (ListItem)listaPorakiKontakt.Items.GetItemAt(0);
                                curr.DataContext = im;
                                MessageBox.Show("vo !null");
                            }
                            else
                            {
                                MessageBox.Show("vo null");
                            }

                            //im.Source = dr.GetValue(i).ToString();
                            //MessageBox.Show(dr.GetValue(i).ToString());
                            dataRow[i] = dr.GetValue(i);

                        }
                        */
                        if (dataRow != null)
                            dataTable.Rows.Add(dataRow);
                    }

                    //listaPorakiKontakt.DataContext = dtSet;


                }
            }
            catch (Exception err)
            {
                MessageBox.Show("Не може да се конектира со базата");
                MessageBox.Show(err.Message);
            }
            finally
            {
                konekcija.Close();
            }
             
        }

        private void btnPratiPoraka_Click(object sender, RoutedEventArgs e)
        {
           // this.IsEnabled = false;
            p = new Poraka();
            SqlConnection konekcija = new SqlConnection(dbConnection);

            //da najdeme Id na selektiranot kontakt od listata na kontakti(treba da go najdeme spored slikata)
            DataRowView drView = (DataRowView)listaKontakti.SelectedItem;
            //MessageBox.Show(drView.Row[2].ToString());
            string slika_kontakt = drView.Row[2].ToString();

            string najdi_kontakti = "select id from Korisnici where slika_url = '" + slika_kontakt + "'";
            SqlCommand komanda_kontakti = new SqlCommand(najdi_kontakti, konekcija);

            try
            {
                konekcija.Open();

                p.IzbranKontaktId.Text = komanda_kontakti.ExecuteScalar().ToString();
                p.KorisnikId.Text = korisnikId.Text;
                p.ShowDialog();  
            }
            catch (Exception err)
            {
                MessageBox.Show("Не може да се конектира со базата");
                MessageBox.Show(err.Message);
            }
            finally
            {
                konekcija.Close();
            }
            
        }

        private void listaPorakiKontakt_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void Grid_IsEnabledChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
        }

        private void Window_IsEnabledChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (smeniEnable == false)
            {
                this.IsEnabled = false;
            }
            else
            {
                this.IsEnabled = true;
                listaPorakiKontakt.Items.Clear();
                listaKontakti.SelectedIndex = -1;
               // MessageBox.Show("Jassss");
            }
        }

        private void Window_GotFocus(object sender, RoutedEventArgs e)
        {
           // MessageBox.Show("Focus");
            //if(p!=null)
           // p.Close();
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if (p != null)
                p.Close();
        }


        private void odjava(object sender, RoutedEventArgs e)
        {


            WindowNajava n = new WindowNajava();
            this.Close();
            n.ShowDialog();


        }


     

      
    }
}
