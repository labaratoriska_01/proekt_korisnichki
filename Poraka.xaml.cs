﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data.SqlClient;
using System.Data;

namespace InterfejsiProekt
{
    /// <summary>
    /// Interaction logic for Poraka.xaml
    /// </summary>
    public partial class Poraka : Window
    {
        string dbConnection = @"Data Source=VLADE\SQLEXPRESS;Initial Catalog=Interfejsi;Integrated Security=True";

        ListBox lb = new ListBox();

        List<String> sodrzina_poraka = new List<String>();
        
        public Poraka()
        {
            InitializeComponent();
            slikaKontakt();
        }

        private void borderLista_Loaded(object sender, RoutedEventArgs e)
        {
            SqlConnection konekcija = new SqlConnection(dbConnection);

            string najdi_sliki = "select url from Slika";
            SqlCommand komanda_sliki = new SqlCommand(najdi_sliki, konekcija);
            DataSet dtSet = new DataSet();


            try
            {
                konekcija.Open();

                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = komanda_sliki;
                adapter.Fill(dtSet, "Slika");
                listaSliki.DataContext = dtSet;
         
            }
            catch (Exception err)
            {
                MessageBox.Show("Не може да се конектира со базата");
                MessageBox.Show(err.Message);
            }
            finally
            {
                konekcija.Close();
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            /*
            foreach (ListItem l in sodrzina_poraka.ListItems)
            {
                MessageBox.Show(l.DataContext.ToString());
            }
            */
            if (lista.Items.Count > 0)
            {
                SqlConnection konekcija = new SqlConnection(dbConnection);

                SqlCommand komanda_prati_poraka = new SqlCommand();
                komanda_prati_poraka.Connection = konekcija;
                komanda_prati_poraka.CommandText = "insert into PrakjaPrima(id_od,id_do,sodrzina_poraka) values(@id_od,@id_do,@sodrzina_poraka)";

                //komanda_prati_poraka.Parameters.Clear();

                komanda_prati_poraka.Parameters.Add(new SqlParameter("@id_od", SqlDbType.Int));
                komanda_prati_poraka.Parameters.Add(new SqlParameter("@id_do", SqlDbType.Int));
                komanda_prati_poraka.Parameters.Add(new SqlParameter("@sodrzina_poraka", SqlDbType.NVarChar));


                komanda_prati_poraka.Parameters["@id_od"].Value = KorisnikId.Text;
                komanda_prati_poraka.Parameters["@id_do"].Value = IzbranKontaktId.Text;

                try
                {
                    konekcija.Open();

                    foreach (string l in lista.Items)
                    {

                        sodrzina_poraka.Add(l);

                        komanda_prati_poraka.Parameters["@sodrzina_poraka"].Value = String.Join(",", sodrzina_poraka);

                        MessageBox.Show(l);

                    }

                    komanda_prati_poraka.ExecuteNonQuery();

                    //izbrishi gi prethodno selektiranite sliki
                    lb.Items.Clear();
                    sodrzina_poraka.Clear();

                }
                catch (Exception err)
                {
                    MessageBox.Show("Не може да се конектира со базата");
                    MessageBox.Show(err.Message);
                }
                finally
                {
                    konekcija.Close();
                }

                //izbrishi gi prethodno selektiranite sliki
                lb.Items.Clear();
                sodrzina_poraka.Clear();
                this.Close();
                
                
            }
            
        }

        private void listaSliki_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            DataRowView drView = (DataRowView)listaSliki.SelectedItem;
            
            ListItem por = new ListItem();

            por.DataContext = drView.Row[0].ToString();
            lista.Items.Add(por.DataContext.ToString());
            
        }

        private void lista_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            int selektiran = (int)lista.SelectedIndex;

            for (int i = 0; i < lista.Items.Count; i++)
            {
                if (i == selektiran)
                {
                    lista.Items.RemoveAt(i);
                }
            }
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {

            if(lista.Items.Count>0)
            {
                lista.Items.Clear();
            }
        }

        private void slikaKontakt()
        {
            SqlConnection konekcija = new SqlConnection(dbConnection);

            string najdi_sliki = "select slika_url from Korisnici where id='"+IzbranKontaktId.Text+"'";
            SqlCommand komanda_sliki = new SqlCommand(najdi_sliki, konekcija);
            DataSet dtSet = new DataSet();


            try
            {
                konekcija.Open();
                //MessageBox.Show("uhuihihi");
                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = komanda_sliki;
                adapter.Fill(dtSet, "Korisnici");
                pratiPoraka.DataContext = dtSet;

            }
            catch (Exception err)
            {
                MessageBox.Show("Не може да се конектира со базата");
                MessageBox.Show(err.Message);
            }
            finally
            {
                konekcija.Close();
            }
        }

        private void pratiPoraka_Loaded(object sender, RoutedEventArgs e)
        {
            slikaKontakt();
        }
    }
}
