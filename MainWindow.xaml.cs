﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Win32;

namespace InterfejsiProekt
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        string dbConnection = @"Data Source=VLADE\SQLEXPRESS;Initial Catalog=Interfejsi;Integrated Security=True";
        string slika_url = String.Empty;

        public MainWindow()
        {
            InitializeComponent();
            fill_lista_Kategorii();
        }
        bool proverkaDete()
        {
            bool exsist = false;
            SqlConnection konekcija = new SqlConnection();
            konekcija.ConnectionString = @"Data Source=VLADE\SQLEXPRESS;Initial Catalog=Interfejsi;Integrated Security=True";
            String komanda = "Select * from Korisnici";
            SqlCommand command = new SqlCommand(komanda, konekcija);
            try
            {
                konekcija.Open();
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    if (reader["username"].ToString().Trim().Equals(txtUsername.Text.Trim()))
                    {
                        exsist = true;

                    }
                }
                reader.Close();
            }
            catch
            {
                MessageBox.Show("Не може да се конектира со базата");
            }
            finally
            {
                konekcija.Close();
            }
            return exsist;
        }

        void fill_Lista_So_Site_Korisnici()
        {
          
            SqlConnection konekcija = new SqlConnection(dbConnection);
            
            try
            {
                konekcija.Open();

                //id_na selektirano dete
                string najdi_id_dete = String.Empty;
                int id_dete = 0;
                SqlCommand komandaNajdiDete;
                if (ddlIzberiDete.SelectedIndex != -1 && ddlIzberiDete.SelectedIndex != 0)
                {
                    najdi_id_dete = "select id from Korisnici where ime = '" + ddlIzberiDete.SelectedValue.ToString() + "'";
                    komandaNajdiDete = new SqlCommand(najdi_id_dete, konekcija);
                    id_dete = Convert.ToInt32(komandaNajdiDete.ExecuteScalar());


                    string korisnici = "Select * from Korisnici where tip = 1 and id not like '" + id_dete + "'";

                    SqlCommand komandaKorisnici = new SqlCommand(korisnici, konekcija);

                    SqlDataReader dr = komandaKorisnici.ExecuteReader();

                    if (listaKorisnici == null)
                    {
                        listaKorisnici = new ListBox();
                    }
                    else
                    {
                        listaKorisnici.Items.Clear();
                        while (dr.Read())
                        {
                            listaKorisnici.Items.Add(dr[1].ToString() + " " + dr[2].ToString());
                        }
                    }
                    dr.Close();
                }
            }
            catch (Exception err)
            {
                MessageBox.Show("Не може да се конектира со базата");
                MessageBox.Show(err.ToString());
            }
            finally
            {
                konekcija.Close();
            }
        }

        void fill_lista_So_Decata_Na_Roditelot()
        {
            SqlConnection konekcija = new SqlConnection(dbConnection);

            try
            {
                konekcija.Open();
                //najdi deca na roditelot
                string deca = "Select * from Korisnici where id_roditel = '"+idRoditel.Text+"'";
                SqlCommand komandaDeca = new SqlCommand(deca, konekcija);
                SqlDataReader dr = komandaDeca.ExecuteReader();


                while (dr.Read())
                {
                    ddlIzberiDete.Items.Add(dr[1].ToString());
                }
                dr.Close();
            }
            catch
            {
                MessageBox.Show("Не може да се конектира со базата");
            }
            finally
            {
                konekcija.Close();
            }
        }

        void re_fill_lista_So_Decata_Na_Roditelot()
        {

            SqlConnection konekcija = new SqlConnection(dbConnection);

            try
            {
                konekcija.Open();
                //najdi deca na roditelot
                string deca = "Select * from Korisnici where id_roditel ='"+idRoditel.Text+"'";
                SqlCommand komandaDeca = new SqlCommand(deca, konekcija);
                SqlDataReader dr = komandaDeca.ExecuteReader();
                if (ddlIzberiDete == null)
                {
                    ddlIzberiDete = new ComboBox();
                }
                else
                {
                    ddlIzberiDete.Items.Clear();
                    ddlIzberiDete.Items.Add("Избери дете");
                    ddlIzberiDete.SelectedIndex = 0;

                    while (dr.Read())
                    {
                        ddlIzberiDete.Items.Add(dr[1].ToString());
                    }
                    dr.Close();
                }
            }
            catch
            {
                MessageBox.Show("Не може да се конектира со базата");
            }
            finally
            {
                konekcija.Close();
            }
        }

        //popolnuvanje na listata so kategorii
        void fill_lista_Kategorii()
        {
            SqlConnection konekcija = new SqlConnection(dbConnection);
            string kategorii = "Select * from Kategorija";

            SqlCommand komandaKategorii = new SqlCommand(kategorii, konekcija);

            try
            {
                konekcija.Open();
                SqlDataReader dr = komandaKategorii.ExecuteReader();

                while (dr.Read())
                {
                    ddlIzberiKategorija.Items.Add(dr["ime"].ToString());
                    //popolni lista so kategorii u tab Prgeled na listi po kategorii
                    ddlIzberiKategorijaZaPregledNaSliki.Items.Add(dr["ime"].ToString());
                }
                dr.Close();
            }
            catch
            {
                MessageBox.Show("Не може да се конектира со базата");
            }
            finally
            {
                konekcija.Close();
            }
        }

        //re_fill lista so kategorii(od koga kje se dodade nova kategorija)
        void re_fill_lista_Kategorii()
        {
            ddlIzberiKategorija.Items.Clear();
            ddlIzberiKategorija.Items.Add("Избери");
            ddlIzberiKategorija.SelectedIndex = 0;

            ddlIzberiKategorijaZaPregledNaSliki.Items.Clear();
            ddlIzberiKategorijaZaPregledNaSliki.Items.Add("Избери");
            ddlIzberiKategorijaZaPregledNaSliki.SelectedIndex = 0;
            SqlConnection konekcija = new SqlConnection(dbConnection);
            string kategorii = "Select * from Kategorija";

            SqlCommand komandaKategorii = new SqlCommand(kategorii, konekcija);

            try
            {
                konekcija.Open();
                SqlDataReader dr = komandaKategorii.ExecuteReader();

                while (dr.Read())
                {
                    ddlIzberiKategorija.Items.Add(dr["ime"].ToString());
                    ddlIzberiKategorijaZaPregledNaSliki.Items.Add(dr["ime"].ToString());
                }
                dr.Close();
            }
            catch
            {
                MessageBox.Show("Не може да се конектира со базата");
            }
            finally
            {
                konekcija.Close();
            }
        }
        void registrirajDete()
        {
            SqlConnection konekcija = new SqlConnection(dbConnection);

            try
            {
                konekcija.Open();

                //dodavanje na nov korisnik(dete)
                string dodadiKorisnik = "INSERT INTO Korisnici(ime,prezime,username,password,tip,slika_url,id_roditel) values('" + txtIme.Text.Trim() + "','" + txtPrezime.Text.Trim() + "','" + txtUsername.Text.Trim() + "','" + txtPassword.Password.Trim() + "',1,'" + slika_url + "','" + idRoditel.Text + "')";
                SqlCommand komandaDodadiNovKorisnik = new SqlCommand(dodadiKorisnik, konekcija);
                komandaDodadiNovKorisnik.ExecuteNonQuery();

                MessageBox.Show("Dodadeno!!!");
            }
            catch (SyntaxErrorException err)
            {
                MessageBox.Show(err.Message.ToString());
            }
            finally
            {
                konekcija.Close();
            }

        }

        private void btnRegistriraj_Click(object sender, RoutedEventArgs e)
        {
            if (proverkaDete())
            {
                errorDete.Visibility = System.Windows.Visibility.Visible;
                errorDete.Text = "Внесете ново корисничко име";
                //errorDete1.Text = " корисничко име";                
            }
            else
            {
                registrirajDete();
                //resetiraj
                izbrishi_info_dete();
            }
            //ispolni lista so deca ODMA!!!
            re_fill_lista_So_Decata_Na_Roditelot();
        }


        //f-ja za da se otvori i da se zachuva izbranata slika vo Resources od proektot
        void open_file_dialog_za_dodavanje_slika()
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Multiselect = false;
            dialog.Title = "Додади слика";
            if (dialog.ShowDialog() == true)
            {

                string folderpath = System.IO.Directory.GetParent(Environment.CurrentDirectory).Parent.FullName + "/Resources/";
                string imeSlika = System.IO.Path.GetFileName(dialog.FileName);

                if (!Directory.Exists(folderpath))
                {
                    Directory.CreateDirectory(folderpath);
                }
                string filePath = folderpath + System.IO.Path.GetFileName(dialog.FileName);
                slika_url = filePath;
                stavenaslika.Text = slika_url;
                System.IO.File.Copy(dialog.FileName, filePath, true);
            }
        }

        //dodavanje na slika za deteto :D
        private void btnDodadiSlika_Click(object sender, RoutedEventArgs e)
        {
            open_file_dialog_za_dodavanje_slika();
        }


        //ispolni ja listata so postoechhki kontakti za selektiranoto dete :D
        void fill_lista_so_Postoechki_Kontakti()
        {
            SqlConnection konekcija = new SqlConnection(dbConnection);

            ComboBoxItem selektiranoDete = new ComboBoxItem();
            if (ddlIzberiDete.SelectedIndex != -1)
            {
                selektiranoDete.Content = ddlIzberiDete.SelectedValue;
            }
            else
            {
                return;
            }

            int id_dete;

            try
            {
                konekcija.Open();

                string najdi_id_dete = "Select id from Korisnici where ime ='" + selektiranoDete.Content.ToString() + "'";
                SqlCommand komanda_najdi_id_dete = new SqlCommand(najdi_id_dete, konekcija);
                id_dete = Convert.ToInt32(komanda_najdi_id_dete.ExecuteScalar());

                string najdi_kontakti = "select a.ime,a.prezime from (select * from Kontakti inner join  Korisnici K on id_kontakt = K.id where id_korisnik = '" + id_dete + "') a";
                SqlCommand komanda_kontakti = new SqlCommand(najdi_kontakti, konekcija);


                SqlDataReader drKontakti = komanda_kontakti.ExecuteReader();

                if (listaPostoeckiKontakti == null)
                {
                    return;
                }
                else
                {
                    listaPostoeckiKontakti.Items.Clear();
                    while (drKontakti.Read())
                    {
                        listaPostoeckiKontakti.Items.Add(drKontakti[0].ToString()+" "+drKontakti[1].ToString());
                    }
                }
            }
            finally
            {
                konekcija.Close();
            }

            //MessageBox.Show(selektiranoDete.Content.ToString());

        }


        //za selektirano dete od drop down listata so deca da ja prikaze slikata od deteto
        void prikazi_slika_Dete()
        {
            SqlConnection konekcija = new SqlConnection(dbConnection);

            string podatociZaDete = string.Empty;
            string pateka = string.Empty;
            ComboBoxItem izbranoDete = new ComboBoxItem();

            if (ddlIzberiDete.SelectedIndex != -1)
            {

                izbranoDete.Content = ddlIzberiDete.SelectedValue.ToString();
                podatociZaDete = "Select * from Korisnici where ime = '" + izbranoDete.Content.ToString() + "'";
            }
            else
            {
                return;
            }

            SqlCommand komandaPodatociDete = new SqlCommand();
            komandaPodatociDete.CommandText = podatociZaDete;
            komandaPodatociDete.Connection = konekcija;

            try
            {
                konekcija.Open();

                SqlDataReader dr = komandaPodatociDete.ExecuteReader();

                while (dr.Read())
                {
                    pateka = dr[6].ToString();
                    if (pateka != "")
                    {
                        slikaDete.Source = (ImageSource)new ImageSourceConverter().ConvertFromString(pateka);
                    }
                    else 
                    {
                        return;
                    }
                }
                dr.Close();
            }
            finally
            {
                konekcija.Close();
            }
        }

        private void ddlIzberiDete_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            if (ddlIzberiDete.SelectedIndex != 0)
            {
                fill_lista_so_Postoechki_Kontakti();
                fill_Lista_So_Site_Korisnici();
                prikazi_slika_Dete();
            }
            /*
            if(ddlIzberiDete.SelectedIndex==0)
            {
                izbrishi_dete_kontakti_korisnici();
            }
            */
        }

        private void btnDodadiKontakt_Click(object sender, RoutedEventArgs e)
        {

            SqlConnection konekcija = new SqlConnection(dbConnection);

            ComboBoxItem izbranoDete = new ComboBoxItem();
            izbranoDete.Content = ddlIzberiDete.SelectedValue.ToString();

            ListBoxItem selektiranKorisnik = new ListBoxItem();
            selektiranKorisnik.Content = listaKorisnici.SelectedValue.ToString();
            string[] novKontaktImePrezime = selektiranKorisnik.Content.ToString().Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);


            try
            {
                konekcija.Open();

                //naogjame id na dete
                string najdi_id_dete = "select id from Korisnici where ime = '" + izbranoDete.Content.ToString() + "'";
                SqlCommand komandaNajdiDete = new SqlCommand(najdi_id_dete, konekcija);
                int id_dete = Convert.ToInt32(komandaNajdiDete.ExecuteScalar());


                //naogjame id na selektiran korisnik
                string najdi_id_nov_kontakt = "select id from Korisnici where ime = '" + novKontaktImePrezime[0].ToString() + "' and prezime = '" + novKontaktImePrezime[1].ToString() + "'";
                SqlCommand komandaNajdiNovKontaktId = new SqlCommand(najdi_id_nov_kontakt, konekcija);
                int id_nov_kontakt = Convert.ToInt32(komandaNajdiNovKontaktId.ExecuteScalar());



                //proverka dali ima vekje takov kontakt
                string redica = "select count(*) from Kontakti where id_korisnik = '" + id_dete + "' and id_kontakt='"+id_nov_kontakt+"'";
                SqlCommand komandaNajdiDuplikat = new SqlCommand(redica, konekcija);
                String najdeno = Convert.ToString( komandaNajdiDuplikat.ExecuteScalar());
                
                SqlCommand komanda_dodadi_vo_kontakti = new SqlCommand();
                komanda_dodadi_vo_kontakti.Connection = konekcija;
                komanda_dodadi_vo_kontakti.CommandText = "insert into Kontakti(id_korisnik,id_kontakt) values(@id_korisnik,@id_kontakt)";

                komanda_dodadi_vo_kontakti.Parameters.AddWithValue("@id_korisnik", SqlDbType.Int).Value = id_dete;
                komanda_dodadi_vo_kontakti.Parameters.AddWithValue("@id_kontakt", SqlDbType.Int).Value = id_nov_kontakt;

                SqlCommand komanda_dodadi_vo_kontakti1 = new SqlCommand();
                komanda_dodadi_vo_kontakti1.Connection = konekcija;
                komanda_dodadi_vo_kontakti1.CommandText = "insert into Kontakti(id_korisnik,id_kontakt) values(@id_korisnik,@id_kontakt)";

                komanda_dodadi_vo_kontakti1.Parameters.AddWithValue("@id_korisnik", SqlDbType.Int).Value = id_nov_kontakt;
                komanda_dodadi_vo_kontakti1.Parameters.AddWithValue("@id_kontakt", SqlDbType.Int).Value = id_dete;


                MessageBox.Show(najdeno.ToString());
                if (najdeno == "0")
                {
                    komanda_dodadi_vo_kontakti.ExecuteNonQuery();
                    komanda_dodadi_vo_kontakti1.ExecuteNonQuery();

                    MessageBox.Show("Успешно додадовте нов контакт");
                }
                else
                {
                    MessageBox.Show("Веќе постои таков контакт!");
                }


            }
            catch (SyntaxErrorException err)
            {
                MessageBox.Show(err.Message.ToString());
            }
            finally
            {
                konekcija.Close();
            }

            //refill lista postoechki korisnici
            fill_lista_so_Postoechki_Kontakti();
        }

        private void listaKorisniciSelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }


        private void ddlIzberiKategorija_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void informaciiZaNajavenKorsnik_Loaded(object sender, RoutedEventArgs e)
        {
            re_fill_lista_So_Decata_Na_Roditelot();
            stavenaslika.Text = String.Empty;
            izbrishi_info_dete();
        }

        private void btnDodadiKategorija_Click(object sender, RoutedEventArgs e)
        {
            SqlConnection konekcija = new SqlConnection(dbConnection);

            try
            {
                konekcija.Open();
                string kategorija = "insert into kategorija(ime) values('"+txtImeKategorija.Text+"')";
                SqlCommand komanda_dodadi_kategorija = new SqlCommand(kategorija, konekcija);
                komanda_dodadi_kategorija.ExecuteNonQuery();
                MessageBox.Show("Dodadena kategorija");
            }
            catch (Exception err)
            {
                MessageBox.Show("Не може да се конектира со базата");
                MessageBox.Show(err.Message);
            }
            finally
            {
                konekcija.Close();
            }
            //re_lill lista kategorii
            re_fill_lista_Kategorii();

            //restart Txt
            izbrishi_kategorija();
        }

        private void btnDodadiNovaSlika_Click(object sender, RoutedEventArgs e)
        {
            open_file_dialog_za_dodavanje_slika();
            if (slika_url != String.Empty)
            {
                dodadenaSlika.Source = new BitmapImage(new Uri(slika_url));
            }
            
        }

        private void btnDodadiSlikaVoBaza_Click(object sender, RoutedEventArgs e)
        {
            SqlConnection konekcija = new SqlConnection(dbConnection);

            try
            {
                konekcija.Open();
                //najdi id na selektirana kategorija
                int id_kategoija = 0;

                if (ddlIzberiKategorija.SelectedIndex == 0)
                {
                    MessageBox.Show("Изберете на која категорија треба да припѓа сликата!!!");
                }
                else
                {
                    string najdi_id_kategorija = "select id from kategorija where ime ='" + ddlIzberiKategorija.SelectedValue + "'";
                    SqlCommand komanda_najdi_id_kategorija = new SqlCommand(najdi_id_kategorija, konekcija);
                    id_kategoija = Convert.ToInt32(komanda_najdi_id_kategorija.ExecuteScalar());


                    //dodadi slika
                    string dodadiSlika = "insert into Slika(opis,url,id_kategorija) values('" + txtOpisSlika.Text + "','" + slika_url + "','" + id_kategoija + "')";
                    SqlCommand komanda_dodadi_slika = new SqlCommand(dodadiSlika, konekcija);
                    komanda_dodadi_slika.ExecuteNonQuery();
                    MessageBox.Show("Dodadena slika");
                }

            }
            catch (Exception err)
            {
                MessageBox.Show("Не може да се конектира со базата");
                MessageBox.Show(err.Message);
            }
            finally
            {
                konekcija.Close();
            }

            //restart
            izbrishi_podatoci_slika();
        }

        private void ddlIzberiKategorijaZaPregledNaSliki_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (ddlIzberiKategorijaZaPregledNaSliki.SelectedIndex != 0 && ddlIzberiKategorijaZaPregledNaSliki.SelectedIndex != -1)
            {

                SqlConnection konekcija = new SqlConnection(dbConnection);

                ComboBoxItem izbranaKategorija = new ComboBoxItem();
                izbranaKategorija.Content = ddlIzberiKategorijaZaPregledNaSliki.SelectedValue.ToString();
                prikaziIzbranaKategorija.Text = ddlIzberiKategorijaZaPregledNaSliki.SelectedValue.ToString();
                DataSet dtSet = new DataSet();

                try
                {
                    konekcija.Open();

                    //naogjame id kategorija
                    string najdi_id_dete = "select id from Kategorija where ime = '" + izbranaKategorija.Content.ToString() + "'";
                    SqlCommand komandaNajdiIdKategorija = new SqlCommand(najdi_id_dete, konekcija);
                    int id_kat = Convert.ToInt32(komandaNajdiIdKategorija.ExecuteScalar());


                    //naogjame id na selektiran korisnik
                    string najdi_ime_sliki = "select url,opis from Slika where id_kategorija = '" + id_kat + "'";
                    SqlCommand komandaNajdiImeSliki = new SqlCommand(najdi_ime_sliki, konekcija);

                    SqlDataAdapter adapterSliki = new SqlDataAdapter();

                    adapterSliki.SelectCommand = komandaNajdiImeSliki;
                    adapterSliki.Fill(dtSet, "Slika");
                    if (listaPregled == null)
                    {
                        listaPregled = new ListBox();
                        listaPregled.DataContext = dtSet;
                    }
                    else
                    {
                        listaPregled.DataContext = dtSet;
                    }

                }
                catch (SyntaxErrorException err)
                {
                    MessageBox.Show(err.Message.ToString());
                }
                finally
                {
                    konekcija.Close();
                }
            }
            
             if (ddlIzberiKategorijaZaPregledNaSliki.SelectedIndex == 0 && listaPregled != null)
            {
                listaPregled.DataContext = null;
                prikaziIzbranaKategorija.Text = String.Empty;
            }
           
        }

        private void izbrishi_info_dete()
        {
            txtIme.Text = " ";
            txtPrezime.Text = " ";
            txtUsername.Text = " ";
            txtPassword.Password = String.Empty;
            stavenaslika.Text = " ";
        }

        private void izbrishi_dete_kontakti_korisnici()
        {
         
                //slikaDete.Source = null;
            
            if (listaPostoeckiKontakti != null && listaKorisnici != null)
            {
                listaKorisnici.DataContext = null;
                listaPostoeckiKontakti.DataContext = null;
            }
        }

        private void izbrishi_podatoci_slika()
        {
            txtOpisSlika.Text = String.Empty;
            ddlIzberiKategorija.SelectedIndex = 0;
            dodadenaSlika.Source = null;
        }

        private void izbrishi_kategorija()
        {
            txtImeKategorija.Text = String.Empty;
        }

        private void TabItem_Loaded(object sender, RoutedEventArgs e)
        {
            izbrishi_dete_kontakti_korisnici();
        }

        private void TabItem_Loaded_1(object sender, RoutedEventArgs e)
        {
            izbrishi_kategorija();
            izbrishi_podatoci_slika();
        }

        private void TabItem_LostFocus(object sender, RoutedEventArgs e)
        {
           // izbrishi_dete_kontakti_korisnici();
            ddlIzberiKategorijaZaPregledNaSliki.SelectedIndex = 0;
        }

        private void TabItem_LostFocus_1(object sender, RoutedEventArgs e)
        {
          // izbrishi_kategorija();
          //  izbrishi_podatoci_slika();
            ddlIzberiKategorijaZaPregledNaSliki.SelectedIndex = 0;
        }

        private void TabItem_LostFocus_2(object sender, RoutedEventArgs e)
        {

            ddlIzberiKategorijaZaPregledNaSliki.SelectedIndex = 0;
        }

        private void najavenRoditel_Loaded(object sender, RoutedEventArgs e)
        {
            izbrishi_dete_kontakti_korisnici();
        }

        private void odjava(object sender, RoutedEventArgs e)
        {

          
            WindowNajava n = new WindowNajava();
            this.Close();
            n.ShowDialog();
           
            
        }
        private void txtUsername_MouseEnter(object sender, MouseEventArgs e)
        {
            errorDete.Visibility = System.Windows.Visibility.Hidden;
        }


    }
}
