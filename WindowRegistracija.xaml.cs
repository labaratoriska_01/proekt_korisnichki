﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data.SqlClient;

namespace InterfejsiProekt
{
    /// <summary>
    /// Interaction logic for WindowRegistracija.xaml
    /// </summary>
    public partial class WindowRegistracija : Window
    {
        public WindowRegistracija()
        {
            InitializeComponent();
        }
        bool proverka()
        {
            bool exsist = false;
            SqlConnection konekcija = new SqlConnection();
            konekcija.ConnectionString = @"Data Source=VLADE\SQLEXPRESS;Initial Catalog=Interfejsi;Integrated Security=True";
            String komanda = "Select * from Korisnici";
            SqlCommand command = new SqlCommand(komanda, konekcija);
            try
            {
                konekcija.Open();
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    if (reader["username"].ToString().Equals(korisnicko.Text))
                    {
                        exsist = true;

                    }

                }
                reader.Close();
            }
            catch
            {
                MessageBox.Show("Не може да се конектира со базата");
            }
            finally
            {
                konekcija.Close();
            }
            return exsist;
        }
        void registriraj_korisnik()
        {
            SqlConnection konekcija = new SqlConnection();
            konekcija.ConnectionString = @"Data Source=VLADE\SQLEXPRESS;Initial Catalog=Interfejsi;Integrated Security=True";
            String komanda = "insert into Korisnici(ime,prezime,username,password,tip) values('" + ime.Text + "','" + prezime.Text + "','" + korisnicko.Text + "','" + lozinka.Password.ToString() + "',0)";
            SqlCommand command = new SqlCommand(komanda, konekcija);

            try
            {
                konekcija.Open();
                command.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                MessageBox.Show("Не може да се конектира со базата");
                MessageBox.Show(e.Message);
            }
            finally
            {
                konekcija.Close();
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (proverka())
            {
                error.Visibility = System.Windows.Visibility.Visible;
                error.Text = "Внесете ново корисничко име";
            }
            else
            {
                registriraj_korisnik();
                this.Close();
            }
        }

        private void ime_MouseEnter(object sender, MouseEventArgs e)
        {
            error.Visibility = System.Windows.Visibility.Hidden;
        }
    }
}
